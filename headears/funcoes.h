#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define USER 0000  			 	//User Default
#define PASS "admin"			//Pass Default

FILE *f;

struct user{
	int user;
	char pass[19];
};

//Cria Avisos de Errro numa caixa
void aviso(char msg[250], char tipo[1]){
	
	int bordas;
	
	bordas = (strlen(msg)/strlen(tipo))+8;
	
	system("CLS");
	
	for(int i=0; i<bordas;i++){
		printf("%s",tipo);
	}
	printf("\n");
	
	printf("%s",tipo);
	printf("   %s   ",msg);
	printf("%s\n",tipo);
	
	for(int i=0; i<bordas;i++){
		printf("%s",tipo);
	}
	printf("\n");
}

//Criar Ficheiro com logins
void criaLogin(int user, char pass[19]){
	
	struct user u;
	
	//printf("%d %s\n",user,pass);
	
	if((f = fopen("login.txt","a"))!=NULL){
		
		u.user = user;
		strcpy(u.pass, pass);
		
		fwrite(&u, sizeof(struct user), 1, f);	
		
		//printf("%d %s\n",u.user,u.pass);
		fclose(f);
		
	}else{
		aviso("Ficheiro impossivel de criar","!");
	}	
}

void criaLogin(){

	struct user u;
	
	if((f = fopen("login.txt","w"))!=NULL){
		
		u.user = USER;
		strcpy(u.pass, PASS);
		
		fwrite(&u, sizeof(struct user), 1, f);	
		
		fclose(f);
		
	}else	
		aviso("Ficheiro impossivel de criar","!");	
}

//Verifica se existe User
int existeUser(int user){
	
	struct user u;
	
	if((f = fopen("login.txt","r"))==NULL){
		fclose(f);
		return 2;
	}
	
	fseek(f, -sizeof(struct user), SEEK_END);
	for(int i=0;i<4;i++){
		fread(&u, sizeof(struct user), 1, f); 
		if(u.user==user)
			return 1;
		printf("%d %s\n",u.user,u.pass);
      	fseek(f, -2*sizeof(struct user), SEEK_CUR);	
	}
	
	return 0;
	
}

//Autentica as credenciais para login com o ficheiro
int autentica(int user, char pass[21]){
	
	struct user u;	
	
	if(strlen(pass)>20){
		return 1;
	}
	
	if(existeUser(user)==0){
		return 0;
	}
	
	if((f = fopen("login.txt","r"))==NULL){
		fclose(f);
		return 3;
	}
	
	fseek(f, -sizeof(struct user), SEEK_END);
	for(int i=0;i<4;i++){
		fread(&u, sizeof(struct user), 1, f);
		
		if(user==u.user){	
			if(strcmp(pass,u.pass)==0){
				fclose(f);
				return -1;
			}else{
				fclose(f);
				return 2;
			}
		}
			
		fseek(f, -2*sizeof(struct user), SEEK_CUR);
	}
	
	fclose(f);
	
}

//Resposta a Erros

//Login
void erroLogin(int error){

	switch (error){
		case 0:
			aviso("User incorreto","!");
			//printf("User incorreto.\n");
			break;
		case 1:
			aviso("Password ultrapassa o limite de caracteres","!");
			//printf("Password ultrapassa o limite de caracteres.\n");
			break;
		case 2:
			aviso("Password Errada","!");
			//printf("Password Errada!\n");
			break;
		case 3:
			criaLogin();
			//aviso("Ficheiro indisponivel","!");
			//printf("Ficheiro indisponivel.");
			break;
		default:
			aviso("Login com Sucesso!"," ");
			//printf("Login com Sucesso!");
			break;
	}
	
}




