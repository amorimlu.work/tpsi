#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void aviso(char msg[250],char tipo[1]);

int main(void){
	inicio:
	char msg[250], tipo[1];
	
	printf("Insira a mensagem de erro: ");
	gets(msg);
	
	aviso(msg, "!");
	
	goto inicio;
}


void aviso(char msg[250], char tipo[1]){
	
	int bordas;
	
	bordas = (strlen(msg)/strlen(tipo))+8;
	
	system("CLS");
	
	for(int i=0; i<bordas;i++){
		printf("%s",tipo);
	}
	printf("\n");
	
	printf("%s",tipo);
	printf("   %s   ",msg);
	printf("%s\n",tipo);
	
	for(int i=0; i<bordas;i++){
		printf("%s",tipo);
	}
	printf("\n");
}

